const express = require('express');
const mongoose = require('mongoose');
const schema = require('./schema/schema');
const bodyParser = require('body-parser');
const cors = require('cors');

const { ApolloServer } = require('apollo-server-express');

const url = 'mongodb://localhost:27017/moviedb';

const connect = mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true});
connect.then((db) => {
    console.log('connected to server successfully');
}, (err) => {
    console.log(err);
})

// Creating apollo server.
const server = new ApolloServer({
    schema: schema
});

const app = express();
app.use(bodyParser.json());
app.use('*', cors());

server.applyMiddleware({app});

app.listen({port: 4000}, () => {
    console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
});