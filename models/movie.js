const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MovieSchema = new Schema(
    {
        name: {
            type: String,
            trim: true,
            required: true
        },
        rating: {
            type: Number,
            required: true
        },
        producer: {
            type: String,
            required: true
        }
    },
    {
        timestamps: true
    }
);

const Movies = mongoose.model('Movie', MovieSchema);
module.exports = {Movies, MovieSchema};